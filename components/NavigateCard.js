import React from "react";
import {SafeAreaView, Text, View, StyleSheet} from "react-native";
import tw from "tailwind-react-native-classnames";
import {GooglePlacesAutocomplete} from "react-native-google-places-autocomplete";
import {GOOGLE_MAPS_APIKEY} from "@env";
import {setDestination, setOrigin} from "../slices/navSlice";
import {useDispatch} from "react-redux";
import {useNavigation} from "@react-navigation/native";


const  NavigateCard = () => {
    const dispatch = useDispatch()
    const navigation = useNavigation()
    return(
       <SafeAreaView style={tw`bg-white flex-1`}>
           <Text style={tw`text-center py-5 text-xl`}>Salut RAPHAEL C4 lelu </Text>
           <View style={tw`border-t border-gray-200 flex-shrink`}>
                <View>
                    <GooglePlacesAutocomplete
                        styles={toInputBoxStyle}
                        query={{
                            key: GOOGLE_MAPS_APIKEY,
                            language: "fr"
                        }}
                        onPress={(data, details) => {
                            dispatch(setDestination({
                                location: details.geometry.location,
                                description: data.description
                            }));
                            navigation.navigate("RideOptionCard")
                        }}
                        returnKeyType={"search"}
                        enablePoweredByContainer={false}
                        fetchDetails={true}
                        minLength={2}
                        placeholder="Where to ?"
                        nearbyPlacesAPI="GooglePlacesSearch"
                        debounce={400}

                    />
                </View>
           </View>
       </SafeAreaView>
    )
}

export default NavigateCard;

const toInputBoxStyle = StyleSheet.create({
    container: {
        backgroundColor: "#fff",
        paddingTop: 20,
        flex: 0
    },
    textInput: {
        backgroundColor: "rgba(134,134,134,0.71)",
        borderRadius: 0,
        fontSize: 18,
        color:"gray"
    },
    textInputContainer: {
        paddingHorizontal: 20,
        paddingBottom: 0
    }
})