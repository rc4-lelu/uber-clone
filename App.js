import React from 'react';
import { StyleSheet} from 'react-native';
import { Provider} from "react-redux"
import {store} from "./store";
import HomeScreen from "./screens/HomeScreen";
import {SafeAreaProvider} from "react-native-safe-area-context/src/SafeAreaContext";
import {NavigationContainer} from "@react-navigation/native";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import MapScreens from "./screens/MapScreens";

export default function App() {
    const Stack = createNativeStackNavigator()
  return (
      <Provider store={store}>
          <NavigationContainer>
              <SafeAreaProvider>
                  <Stack.Navigator>
                      <Stack.Screen
                          options={{headerShown: false}}
                          name="HomeScreen"
                          component={HomeScreen}/>
                      <Stack.Screen
                          options={{headerShown: false}}
                          name="MapScreen"
                          component={MapScreens}/>
                  </Stack.Navigator>
              </SafeAreaProvider>
          </NavigationContainer>
      </Provider>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
